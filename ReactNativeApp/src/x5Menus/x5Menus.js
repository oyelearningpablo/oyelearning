import React, {Component} from 'react';
import PropTypes from "prop-types";
import {StyleSheet, Text, View, TextInput, FlatList, Picker, ScrollView, TouchableHighlight} from 'react-native';
import {Image as ReactImage} from 'react-native';
import Svg, {Defs, Pattern} from 'react-native-svg';
import {Path as SvgPath} from 'react-native-svg';
import {Text as SvgText} from 'react-native-svg';
import {Image as SvgImage} from 'react-native-svg';

export default class X5Menus extends Component {

  constructor(props) {
      super(props);
      this.state = {
          
      };
  }


  handlePress(target, owner) {
    if (this.props.onPress) {
        let name;
        let id;
        let index = -1;
        if (target.search("::") > -1) {
            const varCount = target.split("::").length;
            if (varCount === 2) {
                name = target.split("::")[0];
                id = target.split("::")[1];
            } else if (varCount === 3) {
                name = target.split("::")[0];
                index = parseInt(target.split("::")[1]);
                id = target.split("::")[2];
            }
        } else {
            name = target;
        }
        this.props.onPress({ type: 'button', name: name, index: index, id: id, owner: owner });
    }
  }

  handleChangeTextinput(name, value) {
      let id;
      let index = -1;
      if (name.search('::') > -1) {
          const varCount = name.split("::").length;
          if (varCount === 2) {
              name = name.split("::")[0];
              id = name.split("::")[1];
          } else if (varCount === 3) {
              name = name.split("::")[0];
              index = name.split("::")[1];
              id = name.split("::")[2];
          }
      } else {
          name = name;
      }
      let state = this.state;
      state[name.split('::').join('')] = value;
      this.setState(state, () => {
          if (this.props.onChange) {
              this.props.onChange({ type: 'textinput', name: name, value: value, index: index, id: id });
          }
      });
  }

  render() {
    
    return (
    <ScrollView data-layer="142912cc-dc4e-441e-b3a9-1b705a2c9041" style={styles.x5Menus}>
        <View data-layer="09480a92-700f-44e2-ab12-aeabad426251" style={styles.x5Menus_feed}>
            <View data-layer="e9902724-4880-4172-8d5c-2512583e5b8b" style={styles.x5Menus_feed_rectangulo1012}></View>
            <View data-layer="b5d2c113-27e4-4c94-b45c-429b594c5057" style={styles.x5Menus_feed_rectangulo1013}></View>
            <View data-layer="19bfc647-51f0-4278-bd34-d064d140d04e" style={styles.x5Menus_feed_symbol2046}>
                <Svg data-layer="8922414f-78dc-43a9-9ece-494009e1ae5d" style={styles.x5Menus_feed_symbol2046_trazado144} preserveAspectRatio="none" viewBox="0 0 16 16" fill="rgba(38, 153, 251, 1)"><SvgPath d="M 0 14 L 16 14 L 16 16 L 0 16 L 0 14 Z M 0 7 L 16 7 L 16 12 L 0 12 L 0 7 Z M 7 0 L 16 0 L 16 5 L 7 5 L 7 0 Z M 0 0 L 5 0 L 5 5 L 0 5 L 0 0 Z"  /></Svg>
            </View>
        </View>
        <View data-layer="635800da-41b0-42b4-a4c7-882ddfbaad82" style={styles.x5Menus_explore}>
            <View data-layer="45aae023-1d63-4d22-b001-4002a2c04084" style={styles.x5Menus_explore_rectangulo1014}></View>
            <View data-layer="397134b1-4ce3-4a90-8168-5e5b95235162" style={styles.x5Menus_explore_rectangulo1015}></View>
            <View data-layer="2eaf2fb8-faa9-41fb-944f-24cc9390c509" style={styles.x5Menus_explore_symbol1271}>
                <Svg data-layer="6bea6c08-4367-46a6-b4d5-89307c9d6e05" style={styles.x5Menus_explore_symbol1271_trazado277} preserveAspectRatio="none" viewBox="130 0 16 16" fill="rgba(38, 153, 251, 1)"><SvgPath d="M 138 16 C 133.6000061035156 16 130 12.39999961853027 130 8 C 130 3.600000381469727 133.6000061035156 0 138 0 C 142.3999938964844 0 146 3.599999904632568 146 8 C 146 12.39999961853027 142.3999938964844 16 138 16 Z M 142 4 L 138.1000061035156 5.800000190734863 C 137.2000122070313 6.200000286102295 136.2000122070313 7.200000286102295 135.8000030517578 8.100000381469727 L 134 12 L 137.8999938964844 10.19999980926514 C 138.7999877929688 9.800000190734863 139.7999877929688 8.800000190734863 140.1999969482422 7.899999618530273 L 142 4 Z"  /></Svg>
            </View>
        </View>
        <View data-layer="466abd70-ede4-40f7-bcd8-ca1c26de3e7c" style={styles.x5Menus_messages}>
            <View data-layer="c492b04a-92a3-4015-aed8-02f697c7036f" style={styles.x5Menus_messages_rectangulo1016}></View>
            <View data-layer="963dfc4d-52ba-4e63-8369-af14aa85e14d" style={styles.x5Menus_messages_rectangulo1017}></View>
            <View data-layer="0131da68-c9ae-4334-9fc1-910bd47a1b0b" style={styles.x5Menus_messages_symbol4911}>
                <View data-layer="55aad96c-2ac3-46dc-9bcb-e126b1535aeb" style={styles.x5Menus_messages_symbol4911_rectangulo117}></View>
                <Svg data-layer="793c6886-dfce-4054-be5c-3a7cea215ee6" style={styles.x5Menus_messages_symbol4911_trazado50} preserveAspectRatio="none" viewBox="0 0 16 12" fill="rgba(38, 153, 251, 1)"><SvgPath d="M 14 2 L 2 2 L 8 7 L 14 2 Z M 0 2 C 0 0.8999999761581421 0.8999999761581421 0 2 0 L 14 0 C 15.10000038146973 0 16 0.8999999761581421 16 2 L 16 10 C 16 11.10000038146973 15.10000038146973 12 14 12 L 2 12 C 0.8999999761581421 12 0 11.10000038146973 0 10 L 0 2 Z"  /></Svg>
            </View>
        </View>
        <View data-layer="6dd1a40e-d270-4e80-970f-92e7600a8607" style={styles.x5Menus_notifications}>
            <View data-layer="19b619ce-9653-430a-be8e-6b52bdcabe62" style={styles.x5Menus_notifications_rectangulo1018}></View>
            <View data-layer="20117d3a-f29e-4946-b252-18d2500573ad" style={styles.x5Menus_notifications_rectangulo1019}></View>
            <View data-layer="e8dc91f6-807d-4c75-b410-7234e78e473c" style={styles.x5Menus_notifications_symbol1544}>
                <View data-layer="da7043b4-1b7b-430b-aa0d-0105f5292b0b" style={styles.x5Menus_notifications_symbol1544_rectangulo479}></View>
                <Svg data-layer="13ef364d-5e7f-4a87-927b-0646b7d39692" style={styles.x5Menus_notifications_symbol1544_trazado1} preserveAspectRatio="none" viewBox="0.06250002235174179 0 15.91796875 16" fill="rgba(38, 153, 251, 1)"><SvgPath d="M 15 14 L 10 14 C 10 15.10000038146973 9.100000381469727 16 8 16 C 6.899999618530273 16 6 15.10000038146973 6 14 L 1 14 C 0.6000000238418579 14 0.199999988079071 13.69999980926514 0.1000000238418579 13.30000019073486 C 2.235174179077148e-08 12.90000057220459 0.1000000238418579 12.40000057220459 0.4000000357627869 12.19999980926514 C 1.399999976158142 11.39999961853027 2 10.30000019073486 2 9 L 2 6 C 2 2.700000047683716 4.699999809265137 0 8 0 C 11.30000019073486 0 14 2.700000047683716 14 6 L 14 9 C 14 10.30000019073486 14.60000038146973 11.39999961853027 15.60000038146973 12.19999980926514 C 15.90000057220459 12.5 16.10000038146973 12.89999961853027 15.90000057220459 13.30000019073486 C 15.80000019073486 13.69999980926514 15.39999961853027 14 15 14 Z"  /></Svg>
            </View>
        </View>
        <View data-layer="68de7ff8-061b-4411-8cde-63d36a9c4be4" style={styles.x5Menus_videos}>
            <View data-layer="f5162467-d718-428c-ad57-fd6b9d005927" style={styles.x5Menus_videos_rectangulo1022}></View>
            <View data-layer="0535daa0-40ca-4734-8bd7-3e6fd9475572" style={styles.x5Menus_videos_rectangulo1023}></View>
            <View data-layer="ddb16e44-78eb-4d48-a2f1-668953778bc0" style={styles.x5Menus_videos_symbol1301}>
                <View data-layer="4d9502b7-2dd4-45a3-9c40-a8f560faeda9" style={styles.x5Menus_videos_symbol1301_rectangulo312}></View>
                <Svg data-layer="cfb588fc-5354-4008-b284-e633832b4947" style={styles.x5Menus_videos_symbol1301_trazado123} preserveAspectRatio="none" viewBox="-2.9802322387695312e-8 1.9868215517249155e-8 16 10" fill="rgba(38, 153, 251, 1)"><SvgPath d="M 16 0.4000000655651093 L 16 9.733333587646484 C 16 9.866666793823242 15.86666679382324 10 15.73333358764648 10 C 15.73333358764648 10 15.60000038146973 10 15.60000038146973 10 C 15.46666622161865 10 15.46666622161865 10 15.33333396911621 9.866666793823242 L 12 6.533333778381348 L 12 9.066667556762695 C 12 9.333333969116211 11.86666679382324 9.466667175292969 11.73333358764648 9.733333587646484 C 11.46666717529297 9.866666793823242 11.33333396911621 10 10.93333339691162 10 L 1.066666722297668 10 C 0.8000000715255737 10 0.5333333611488342 9.866666793823242 0.4000000357627869 9.733333587646484 C 0.1333333402872086 9.466667175292969 0 9.333333969116211 0 9.066667556762695 L 0 1.066666960716248 C 0 0.8000003099441528 0.1333333402872086 0.5333335995674133 0.2666666805744171 0.4000003039836884 C 0.5333333611488342 0.1333333551883698 0.6666666865348816 1.986821551724915e-08 1.066666722297668 1.986821551724915e-08 L 11.0666675567627 1.986821551724915e-08 C 11.33333396911621 1.986821551724915e-08 11.60000038146973 0.1333333551883698 11.73333358764648 0.2666667103767395 C 11.86666679382324 0.5333333611488342 12 0.8000000715255737 12 1.066666722297668 L 12 3.600000143051147 L 15.46666717529297 0.1333335489034653 C 15.46666717529297 1.986821551724915e-08 15.60000038146973 1.986821551724915e-08 15.73333358764648 1.986821551724915e-08 C 15.86666679382324 0.1333333551883698 16 0.1333333551883698 16 0.4000000655651093 Z"  /></Svg>
            </View>
        </View>
        <View data-layer="15778955-5648-47fe-81ff-36f0588b3539" style={styles.x5Menus_places}>
            <View data-layer="fe3bca39-ff9e-405e-8852-59564663d91a" style={styles.x5Menus_places_rectangulo1024}></View>
            <View data-layer="787071a0-e369-46f9-9a77-3fdeea2125f7" style={styles.x5Menus_places_rectangulo1025}></View>
            <View data-layer="aafdf406-c151-4afa-8757-211c6f09c0fb" style={styles.x5Menus_places_symbol13119}>
                <View data-layer="4f4017e2-316e-46fa-90ca-75f341f9d428" style={styles.x5Menus_places_symbol13119_rectangulo305}></View>
                <Svg data-layer="0b049d19-edba-47bb-a43d-5665e0262aee" style={styles.x5Menus_places_symbol13119_trazado114} preserveAspectRatio="none" viewBox="0.02500009536743164 -0.05000007152557373 13.1837158203125 16" fill="rgba(38, 153, 251, 1)"><SvgPath d="M 6.579516887664795 9.470250129699707 C 8.174532890319824 9.470250129699707 9.370794296264648 8.174299240112305 9.370794296264648 6.678971767425537 C 9.370794296264648 5.183644771575928 8.174532890319824 3.78800630569458 6.579516887664795 3.78800630569458 C 4.984501361846924 3.78800630569458 3.7882399559021 5.083956241607666 3.7882399559021 6.579283237457275 C 3.7882399559021 8.074611663818359 5.084189891815186 9.470250129699707 6.579516887664795 9.470250129699707 Z M 1.894158840179443 1.893925189971924 C 4.486059188842773 -0.6979750394821167 8.672975540161133 -0.6979750394821167 11.2648754119873 1.893925189971924 C 13.85677623748779 4.485825538635254 13.85677623748779 8.672741889953613 11.2648754119873 11.26464176177979 L 6.579516887664795 15.95000076293945 L 1.894158959388733 11.2646427154541 C -0.598052978515625 8.672741889953613 -0.598052978515625 4.485825538635254 1.894158840179443 1.893925189971924 Z"  /></Svg>
            </View>
        </View>
        <View data-layer="7fdf6d8e-f7fe-4297-bfad-186eb4bae293" style={styles.x5Menus_settings}>
            <View data-layer="77ee19a2-3539-4e2e-8e96-c477effcaae2" style={styles.x5Menus_settings_rectangulo1026}></View>
            <View data-layer="cef429b4-42f5-4c63-9ccc-60700c4f73e4" style={styles.x5Menus_settings_rectangulo1027}></View>
            <View data-layer="41a61da4-71c4-4628-b48f-15f91d572f2d" style={styles.x5Menus_settings_symbol14711}>
                <View data-layer="922cb692-eb4e-4179-842d-fbc17dd4af05" style={styles.x5Menus_settings_symbol14711_rectangulo193}></View>
                <Svg data-layer="1aa66db2-190c-495c-89fa-b40970b45db3" style={styles.x5Menus_settings_symbol14711_trazado104} preserveAspectRatio="none" viewBox="260 0 15.199951171875 16" fill="rgba(38, 153, 251, 1)"><SvgPath d="M 267.5 10 C 268.6000061035156 10 269.5 9.100000381469727 269.5 8 C 269.5 6.899999618530273 268.6000061035156 6 267.5 6 C 266.3999938964844 6 265.5 6.900000095367432 265.5 8 C 265.5 9.100000381469727 266.3999938964844 10 267.5 10 Z M 263.3999938964844 3.599999904632568 C 264.1000061035156 3 264.8999938964844 2.5 265.7999877929688 2.299999952316284 L 266.5999755859375 0 L 268.5999755859375 0 L 269.3999633789063 2.299999952316284 C 270.2999572753906 2.599999904632568 271.0999755859375 3 271.7999572753906 3.599999904632568 L 274.199951171875 3.099999904632568 L 275.199951171875 4.899999618530273 L 273.5999450683594 6.699999809265137 C 273.699951171875 7.099999904632568 273.699951171875 7.599999904632568 273.699951171875 8 C 273.699951171875 8.399999618530273 273.5999450683594 8.899999618530273 273.5999450683594 9.300000190734863 L 275.199951171875 11.10000038146973 L 274.199951171875 12.90000057220459 L 271.7999572753906 12.40000057220459 C 271.0999450683594 13.00000095367432 270.2999572753906 13.50000095367432 269.3999633789063 13.70000076293945 L 268.5999755859375 16 L 266.5999755859375 16 L 265.7999877929688 13.69999980926514 C 264.8999938964844 13.39999961853027 264.0999755859375 13 263.3999938964844 12.39999961853027 L 261 12.89999961853027 L 260 11.09999942779541 L 261.6000061035156 9.299999237060547 C 261.5 8.899999618530273 261.5 8.399999618530273 261.5 7.999999046325684 C 261.5 7.599998474121094 261.6000061035156 7.099998950958252 261.6000061035156 6.69999885559082 L 260 4.900000095367432 L 261 3.100000143051147 L 263.3999938964844 3.599999904632568 Z"  /></Svg>
            </View>
        </View>
        <View data-layer="b01fa04f-bf15-4626-90d6-e73afadf3192" style={styles.x5Menus_search}>
            <View data-layer="715f5e84-0e30-4129-896a-c15d3ff07615" style={styles.x5Menus_search_rectangulo1028}></View>
            <View data-layer="2c6877fc-0af1-4c48-a91f-205b03c1599c" style={styles.x5Menus_search_symbol6216}>
                <View data-layer="cf91e964-cc05-4491-89cf-c3e26f3dc16b" style={styles.x5Menus_search_symbol6216_rectangulo176}></View>
                <Svg data-layer="3a4a0349-23b4-48f4-9cae-b0615d575a6f" style={styles.x5Menus_search_symbol6216_trazado99} preserveAspectRatio="none" viewBox="0 0 15.9000244140625 15.89990234375" fill="rgba(38, 153, 251, 1)"><SvgPath d="M 15.89999961853027 14.5 L 12.59999942779541 11.19999980926514 C 13.5 10 14 8.600000381469727 14 7 C 14 3.099999904632568 10.89999961853027 0 7 0 C 3.100000381469727 0 0 3.099999904632568 0 7 C 0 10.89999961853027 3.099999904632568 14 7 14 C 8.600000381469727 14 10 13.5 11.19999980926514 12.60000038146973 L 14.5 15.90000057220459 L 15.89999961853027 14.5 Z M 2 7 C 2 4.199999809265137 4.199999809265137 2 7 2 C 9.800000190734863 2 12 4.199999809265137 12 7 C 12 9.800000190734863 9.800000190734863 12 7 12 C 4.199999809265137 12 2 9.800000190734863 2 7 Z"  /></Svg>
            </View>
        </View>
        <View data-layer="f2fd562a-500b-478c-baaa-11192de5a8b6" style={styles.x5Menus_photos}>
            <View data-layer="46c70a60-9551-4543-8a03-0c6d062886d5" style={styles.x5Menus_photos_rectangulo1020}></View>
            <View data-layer="44df23ea-376c-4805-b3f9-dd17c9ca8060" style={styles.x5Menus_photos_symbol1281}>
                <View data-layer="372536b4-5f33-4b92-9e5f-9f5eb6b60a3a" style={styles.x5Menus_photos_symbol1281_rectangulo549}></View>
                <Svg data-layer="be06d8b5-9cf7-4dab-8558-0d865cab3714" style={styles.x5Menus_photos_symbol1281_trazado148} preserveAspectRatio="none" viewBox="0 -2.9802322387695312e-8 16 12" fill="rgba(38, 153, 251, 1)"><SvgPath d="M 14.93333339691162 0 C 15.19999980926514 0 15.46666622161865 0.1333333402872086 15.60000038146973 0.2666666805744171 C 15.86666679382324 0.5333333611488342 16 0.6666666865348816 16 1.066666722297668 L 16 11.0666675567627 C 16 11.33333396911621 15.86666679382324 11.60000038146973 15.73333358764648 11.73333358764648 C 15.46666717529297 11.86666679382324 15.33333396911621 12 14.93333339691162 12 L 1.066666722297668 12 C 0.6666666865348816 12 0.5333333611488342 11.86666679382324 0.2666666805744171 11.73333358764648 C 0.1333333402872086 11.46666717529297 0 11.33333396911621 0 10.93333339691162 L 0 1.066666722297668 C 0 0.8000000715255737 0.1333333402872086 0.5333333611488342 0.2666666805744171 0.4000000357627869 C 0.5333333611488342 0.1333333402872086 0.6666666865348816 0 1.066666722297668 0 L 14.93333339691162 0 Z M 4.533333778381348 2.400000095367432 C 4.266666889190674 2.133333444595337 3.866666793823242 2 3.466666698455811 2 C 3.0666663646698 2 2.666666746139526 2.133333444595337 2.400000095367432 2.400000095367432 C 2.133333444595337 2.666666746139526 2 3.066666603088379 2 3.466666698455811 C 2 3.866666555404663 2.133333444595337 4.266666412353516 2.400000095367432 4.533333301544189 C 2.666666507720947 4.800000190734863 3.066666603088379 4.933333396911621 3.466666698455811 4.933333396911621 C 3.866666555404663 4.933333396911621 4.266666412353516 4.800000190734863 4.533333301544189 4.533333301544189 C 4.800000190734863 4.266666412353516 4.933333396911621 3.866666555404663 4.933333396911621 3.466666698455811 C 4.933333396911621 3.066666603088379 4.800000190734863 2.666666746139526 4.533333778381348 2.400000095367432 Z M 14 10 L 10 4 L 6.40000057220459 8.666666984558105 L 4 6.933333396911621 L 2 10 L 14 10 Z"  /></Svg>
            </View>
        </View>
    </ScrollView>
    );
  }
}

X5Menus.propTypes = {

}

X5Menus.defaultProps = {

}


const styles = StyleSheet.create({
  "x5Menus": {
    "opacity": 1,
    "position": "relative",
    "backgroundColor": "rgba(38, 153, 251, 1)",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 375,
    "height": 667,
    "left": 0,
    "top": 0
  },
  "x5Menus_feed": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "transparent",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 74,
    "height": 74,
    "left": 0,
    "top": 0
  },
  "x5Menus_feed_rectangulo1012": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "rgba(255, 255, 255, 1)",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "borderTopLeftRadius": 0,
    "borderTopRightRadius": 0,
    "borderBottomLeftRadius": 0,
    "borderBottomRightRadius": 0,
    "width": 74,
    "height": 74,
    "left": 0,
    "top": 0
  },
  "x5Menus_feed_rectangulo1013": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "rgba(241, 249, 255, 1)",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "borderTopLeftRadius": 0,
    "borderTopRightRadius": 0,
    "borderBottomLeftRadius": 0,
    "borderBottomRightRadius": 0,
    "width": 74,
    "height": 1,
    "left": 0,
    "top": 73
  },
  "x5Menus_feed_symbol2046": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "transparent",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 16,
    "height": 16,
    "left": 29,
    "top": 29
  },
  "x5Menus_feed_symbol2046_trazado144": {
    "opacity": 1,
    "position": "absolute",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 16,
    "height": 16,
    "left": 0,
    "top": 0
  },
  "x5Menus_explore": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "transparent",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 74,
    "height": 74,
    "left": 0,
    "top": 74
  },
  "x5Menus_explore_rectangulo1014": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "rgba(255, 255, 255, 1)",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "borderTopLeftRadius": 0,
    "borderTopRightRadius": 0,
    "borderBottomLeftRadius": 0,
    "borderBottomRightRadius": 0,
    "width": 74,
    "height": 74,
    "left": 0,
    "top": 0
  },
  "x5Menus_explore_rectangulo1015": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "rgba(241, 249, 255, 1)",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "borderTopLeftRadius": 0,
    "borderTopRightRadius": 0,
    "borderBottomLeftRadius": 0,
    "borderBottomRightRadius": 0,
    "width": 74,
    "height": 1,
    "left": 0,
    "top": 73
  },
  "x5Menus_explore_symbol1271": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "transparent",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 16,
    "height": 16,
    "left": 29,
    "top": 29
  },
  "x5Menus_explore_symbol1271_trazado277": {
    "opacity": 1,
    "position": "absolute",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 16,
    "height": 16,
    "left": 0,
    "top": 0
  },
  "x5Menus_messages": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "transparent",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 74,
    "height": 74,
    "left": 0,
    "top": 148
  },
  "x5Menus_messages_rectangulo1016": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "rgba(255, 255, 255, 1)",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "borderTopLeftRadius": 0,
    "borderTopRightRadius": 0,
    "borderBottomLeftRadius": 0,
    "borderBottomRightRadius": 0,
    "width": 74,
    "height": 74,
    "left": 0,
    "top": 0
  },
  "x5Menus_messages_rectangulo1017": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "rgba(241, 249, 255, 1)",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "borderTopLeftRadius": 0,
    "borderTopRightRadius": 0,
    "borderBottomLeftRadius": 0,
    "borderBottomRightRadius": 0,
    "width": 74,
    "height": 1,
    "left": 0,
    "top": 73
  },
  "x5Menus_messages_symbol4911": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "transparent",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 16,
    "height": 16,
    "left": 29,
    "top": 29
  },
  "x5Menus_messages_symbol4911_rectangulo117": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "transparent",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "borderTopLeftRadius": 0,
    "borderTopRightRadius": 0,
    "borderBottomLeftRadius": 0,
    "borderBottomRightRadius": 0,
    "width": 16,
    "height": 16,
    "left": 0,
    "top": 0
  },
  "x5Menus_messages_symbol4911_trazado50": {
    "opacity": 1,
    "position": "absolute",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 16,
    "height": 12,
    "left": 0,
    "top": 2
  },
  "x5Menus_notifications": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "transparent",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 74,
    "height": 74,
    "left": 0,
    "top": 222
  },
  "x5Menus_notifications_rectangulo1018": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "rgba(255, 255, 255, 1)",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "borderTopLeftRadius": 0,
    "borderTopRightRadius": 0,
    "borderBottomLeftRadius": 0,
    "borderBottomRightRadius": 0,
    "width": 74,
    "height": 74,
    "left": 0,
    "top": 0
  },
  "x5Menus_notifications_rectangulo1019": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "rgba(241, 249, 255, 1)",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "borderTopLeftRadius": 0,
    "borderTopRightRadius": 0,
    "borderBottomLeftRadius": 0,
    "borderBottomRightRadius": 0,
    "width": 74,
    "height": 1,
    "left": 0,
    "top": 73
  },
  "x5Menus_notifications_symbol1544": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "transparent",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 16,
    "height": 16,
    "left": 29,
    "top": 29
  },
  "x5Menus_notifications_symbol1544_rectangulo479": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "transparent",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "borderTopLeftRadius": 0,
    "borderTopRightRadius": 0,
    "borderBottomLeftRadius": 0,
    "borderBottomRightRadius": 0,
    "width": 16,
    "height": 16,
    "left": 0,
    "top": 0
  },
  "x5Menus_notifications_symbol1544_trazado1": {
    "opacity": 1,
    "position": "absolute",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 15.92,
    "height": 16,
    "left": 0,
    "top": 0
  },
  "x5Menus_videos": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "transparent",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 74,
    "height": 74,
    "left": 0,
    "top": 370
  },
  "x5Menus_videos_rectangulo1022": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "rgba(255, 255, 255, 1)",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "borderTopLeftRadius": 0,
    "borderTopRightRadius": 0,
    "borderBottomLeftRadius": 0,
    "borderBottomRightRadius": 0,
    "width": 74,
    "height": 74,
    "left": 0,
    "top": 0
  },
  "x5Menus_videos_rectangulo1023": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "rgba(241, 249, 255, 1)",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "borderTopLeftRadius": 0,
    "borderTopRightRadius": 0,
    "borderBottomLeftRadius": 0,
    "borderBottomRightRadius": 0,
    "width": 74,
    "height": 1,
    "left": 0,
    "top": 73
  },
  "x5Menus_videos_symbol1301": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "transparent",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 16,
    "height": 16,
    "left": 29,
    "top": 29
  },
  "x5Menus_videos_symbol1301_rectangulo312": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "transparent",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "borderTopLeftRadius": 0,
    "borderTopRightRadius": 0,
    "borderBottomLeftRadius": 0,
    "borderBottomRightRadius": 0,
    "width": 16,
    "height": 16,
    "left": 0,
    "top": 0
  },
  "x5Menus_videos_symbol1301_trazado123": {
    "opacity": 1,
    "position": "absolute",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 16,
    "height": 10,
    "left": 0,
    "top": 3
  },
  "x5Menus_places": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "transparent",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 74,
    "height": 74,
    "left": 0,
    "top": 444
  },
  "x5Menus_places_rectangulo1024": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "rgba(255, 255, 255, 1)",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "borderTopLeftRadius": 0,
    "borderTopRightRadius": 0,
    "borderBottomLeftRadius": 0,
    "borderBottomRightRadius": 0,
    "width": 74,
    "height": 74,
    "left": 0,
    "top": 0
  },
  "x5Menus_places_rectangulo1025": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "rgba(241, 249, 255, 1)",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "borderTopLeftRadius": 0,
    "borderTopRightRadius": 0,
    "borderBottomLeftRadius": 0,
    "borderBottomRightRadius": 0,
    "width": 74,
    "height": 1,
    "left": 0,
    "top": 73
  },
  "x5Menus_places_symbol13119": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "transparent",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 16,
    "height": 16,
    "left": 29,
    "top": 29
  },
  "x5Menus_places_symbol13119_rectangulo305": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "transparent",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "borderTopLeftRadius": 0,
    "borderTopRightRadius": 0,
    "borderBottomLeftRadius": 0,
    "borderBottomRightRadius": 0,
    "width": 16,
    "height": 16,
    "left": 0,
    "top": 0
  },
  "x5Menus_places_symbol13119_trazado114": {
    "opacity": 1,
    "position": "absolute",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 13.18,
    "height": 16,
    "left": 1,
    "top": 0
  },
  "x5Menus_settings": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "transparent",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 74,
    "height": 74,
    "left": 0,
    "top": 518
  },
  "x5Menus_settings_rectangulo1026": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "rgba(255, 255, 255, 1)",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "borderTopLeftRadius": 0,
    "borderTopRightRadius": 0,
    "borderBottomLeftRadius": 0,
    "borderBottomRightRadius": 0,
    "width": 74,
    "height": 74,
    "left": 0,
    "top": 0
  },
  "x5Menus_settings_rectangulo1027": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "rgba(241, 249, 255, 1)",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "borderTopLeftRadius": 0,
    "borderTopRightRadius": 0,
    "borderBottomLeftRadius": 0,
    "borderBottomRightRadius": 0,
    "width": 74,
    "height": 1,
    "left": 0,
    "top": 73
  },
  "x5Menus_settings_symbol14711": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "transparent",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 16,
    "height": 16,
    "left": 30,
    "top": 29
  },
  "x5Menus_settings_symbol14711_rectangulo193": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "transparent",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "borderTopLeftRadius": 0,
    "borderTopRightRadius": 0,
    "borderBottomLeftRadius": 0,
    "borderBottomRightRadius": 0,
    "width": 16,
    "height": 16,
    "left": 0,
    "top": 0
  },
  "x5Menus_settings_symbol14711_trazado104": {
    "opacity": 1,
    "position": "absolute",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 15.2,
    "height": 16,
    "left": 0,
    "top": 0
  },
  "x5Menus_search": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "transparent",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 74,
    "height": 75,
    "left": 0,
    "top": 592
  },
  "x5Menus_search_rectangulo1028": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "rgba(255, 255, 255, 1)",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "borderTopLeftRadius": 0,
    "borderTopRightRadius": 0,
    "borderBottomLeftRadius": 0,
    "borderBottomRightRadius": 0,
    "width": 74,
    "height": 75,
    "left": 0,
    "top": 0
  },
  "x5Menus_search_symbol6216": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "transparent",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 16,
    "height": 16,
    "left": 29,
    "top": 30
  },
  "x5Menus_search_symbol6216_rectangulo176": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "transparent",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "borderTopLeftRadius": 0,
    "borderTopRightRadius": 0,
    "borderBottomLeftRadius": 0,
    "borderBottomRightRadius": 0,
    "width": 16,
    "height": 16,
    "left": 0,
    "top": 0
  },
  "x5Menus_search_symbol6216_trazado99": {
    "opacity": 1,
    "position": "absolute",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 15.9,
    "height": 15.9,
    "left": 0,
    "top": 0
  },
  "x5Menus_photos": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "transparent",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 94,
    "height": 94,
    "left": 0,
    "top": 286
  },
  "x5Menus_photos_rectangulo1020": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "rgba(255, 255, 255, 1)",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "borderTopLeftRadius": 0,
    "borderTopRightRadius": 0,
    "borderBottomLeftRadius": 0,
    "borderBottomRightRadius": 0,
    "shadowColor": "rgb(38,  153,  251)",
    "shadowOpacity": 0.2,
    "shadowOffset": {
      "width": 0,
      "height": 6
    },
    "shadowRadius": 20,
    "width": 94,
    "height": 94,
    "left": 0,
    "top": 0
  },
  "x5Menus_photos_symbol1281": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "transparent",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 16,
    "height": 16,
    "left": 39,
    "top": 39
  },
  "x5Menus_photos_symbol1281_rectangulo549": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "transparent",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "borderTopLeftRadius": 0,
    "borderTopRightRadius": 0,
    "borderBottomLeftRadius": 0,
    "borderBottomRightRadius": 0,
    "width": 16,
    "height": 16,
    "left": 0,
    "top": 0
  },
  "x5Menus_photos_symbol1281_trazado148": {
    "opacity": 1,
    "position": "absolute",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 16,
    "height": 12,
    "left": 0,
    "top": 2
  }
});