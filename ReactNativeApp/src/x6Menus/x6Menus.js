import React, {Component} from 'react';
import PropTypes from "prop-types";
import {StyleSheet, Text, View, TextInput, FlatList, Picker, ScrollView, TouchableHighlight} from 'react-native';
import {Image as ReactImage} from 'react-native';
import Svg, {Defs, Pattern} from 'react-native-svg';
import {Path as SvgPath} from 'react-native-svg';
import {Text as SvgText} from 'react-native-svg';
import {Image as SvgImage} from 'react-native-svg';

export default class X6Menus extends Component {

  constructor(props) {
      super(props);
      this.state = {
          
      };
  }


  handlePress(target, owner) {
    if (this.props.onPress) {
        let name;
        let id;
        let index = -1;
        if (target.search("::") > -1) {
            const varCount = target.split("::").length;
            if (varCount === 2) {
                name = target.split("::")[0];
                id = target.split("::")[1];
            } else if (varCount === 3) {
                name = target.split("::")[0];
                index = parseInt(target.split("::")[1]);
                id = target.split("::")[2];
            }
        } else {
            name = target;
        }
        this.props.onPress({ type: 'button', name: name, index: index, id: id, owner: owner });
    }
  }

  handleChangeTextinput(name, value) {
      let id;
      let index = -1;
      if (name.search('::') > -1) {
          const varCount = name.split("::").length;
          if (varCount === 2) {
              name = name.split("::")[0];
              id = name.split("::")[1];
          } else if (varCount === 3) {
              name = name.split("::")[0];
              index = name.split("::")[1];
              id = name.split("::")[2];
          }
      } else {
          name = name;
      }
      let state = this.state;
      state[name.split('::').join('')] = value;
      this.setState(state, () => {
          if (this.props.onChange) {
              this.props.onChange({ type: 'textinput', name: name, value: value, index: index, id: id });
          }
      });
  }

  render() {
    
    return (
    <ScrollView data-layer="e610fe6f-7ebf-4553-bd39-16b600c12650" style={styles.x6Menus}>
        <View data-layer="9129a784-26e4-4e5f-9efd-21204a56c03c" style={styles.x6Menus_topsaebbfea4}>
            <View data-layer="665b8c95-e6fd-4bc1-a7f9-5e05053adc08" style={styles.x6Menus_topsaebbfea4_rectangulo1007}></View>
            <View data-layer="164d9298-0f5a-4630-8c92-60d424ce57f2" style={styles.x6Menus_topsaebbfea4_icon0f69b727}>
                <Svg data-layer="929762c6-2335-4f45-9a4c-f1b5419fb6be" style={styles.x6Menus_topsaebbfea4_icon0f69b727_elipse243} preserveAspectRatio="none" viewBox="0 0 80 80" fill="rgba(188, 224, 253, 1)"><SvgPath d="M 40 0 C 62.09139251708984 0 80 17.90860939025879 80 40 C 80 62.09139251708984 62.09139251708984 80 40 80 C 17.90860939025879 80 0 62.09139251708984 0 40 C 0 17.90860939025879 17.90860939025879 0 40 0 Z"  /></Svg>
                <Svg data-layer="2cf90da1-9fbb-4a8d-9226-5a5eb4ee1a9c" style={styles.x6Menus_topsaebbfea4_icon0f69b727_trazado234} preserveAspectRatio="none" viewBox="80 0 40 68.2998046875" fill="rgba(38, 153, 251, 1)"><SvgPath d="M 80 0 L 80 40 L 108.3000106811523 68.30001068115234 C 115.5 61.05000686645508 120 51.05000305175781 120 40 C 120 17.90000152587891 102.1000137329102 0 80 0 Z"  /></Svg>
            </View>
            <Text data-layer="d6648c61-5515-4639-b50d-d426cec1f5d8" style={styles.x6Menus_topsaebbfea4_tops}>TOPS</Text>
        </View>
        <View data-layer="9feb497e-bc2a-445b-b1e7-51e3afe52ddc" style={styles.x6Menus_bottomsa866578a}>
            <View data-layer="09a80775-69cc-47dc-8d50-2a4b47768432" style={styles.x6Menus_bottomsa866578a_rectangulo1008}></View>
            <View data-layer="6d74af48-9a88-4c3e-8fc5-c5566b588b86" style={styles.x6Menus_bottomsa866578a_icon8382e0b8}>
                <Svg data-layer="e1d56dd7-9cdd-4f52-b81b-c239de84adc8" style={styles.x6Menus_bottomsa866578a_icon8382e0b8_trazado235} preserveAspectRatio="none" viewBox="642.9000244140625 0 92.35009765625 80" fill="rgba(188, 224, 253, 1)"><SvgPath d="M 712.1500854492188 0 L 666 0 L 642.9000244140625 40 L 666 80 L 712.1500854492188 80 L 735.2500610351563 40 L 712.1500854492188 0 Z"  /></Svg>
                <Svg data-layer="189a8c23-5b0e-42d9-9268-cd608921ec94" style={styles.x6Menus_bottomsa866578a_icon8382e0b8_trazado236} preserveAspectRatio="none" viewBox="642.9000244140625 0 92.35009765625 80" fill="rgba(38, 153, 251, 1)"><SvgPath d="M 712.1500854492188 0 L 666 80 L 642.9000244140625 40 L 735.2500610351563 40 L 712.1500854492188 0 Z"  /></Svg>
            </View>
            <Text data-layer="cc73fac6-2f26-49a8-ae97-28d9ad7c49c6" style={styles.x6Menus_bottomsa866578a_bottoms}>BOTTOMS</Text>
        </View>
        <View data-layer="abe5c47a-7fc5-4a41-a8d9-bdad8bc15684" style={styles.x6Menus_accessories88b62596}>
            <View data-layer="ff30e1b0-46d9-4a82-b5fd-4cc110f42b25" style={styles.x6Menus_accessories88b62596_rectangulo1009}></View>
            <View data-layer="22cb4dbd-2a46-40e9-98de-7f336eb492b5" style={styles.x6Menus_accessories88b62596_icon051b936a}>
                <View data-layer="2055aa5c-6a82-4d76-8f0d-9f8692fc528d" style={styles.x6Menus_accessories88b62596_icon051b936a_rectangulo874}></View>
                <View data-layer="d7920c21-c562-480e-ae78-59d3c56471a8" style={styles.x6Menus_accessories88b62596_icon051b936a_rectangulo875}></View>
                <View data-layer="688c4223-bd7e-42c3-9f07-5c37eb57ede4" style={styles.x6Menus_accessories88b62596_icon051b936a_rectangulo876}></View>
                <View data-layer="45ef2b73-6279-4294-a6d4-6dd2984a1013" style={styles.x6Menus_accessories88b62596_icon051b936a_rectangulo877}></View>
            </View>
            <Text data-layer="69ceed36-be0a-4cf3-a9af-cef93043b39a" style={styles.x6Menus_accessories88b62596_accessories}>ACCESSORIES</Text>
        </View>
        <View data-layer="c7b9b4cc-fc83-4555-8fd4-e8a7a7f7b36e" style={styles.x6Menus_morea31b726a}>
            <View data-layer="feceaaa3-d9a2-4a00-8c78-8dfeb4f59b0f" style={styles.x6Menus_morea31b726a_rectangulo1010}></View>
            <View data-layer="f0938bc4-e559-469a-9149-8b27faff2080" style={styles.x6Menus_morea31b726a_icon}>
                <View data-layer="6a29376d-0b14-42a0-b646-d7c3658b3f52" style={styles.x6Menus_morea31b726a_icon_rectangulo1011}></View>
                <Svg data-layer="590bf9ae-62f8-40a6-8536-f1e0a47d2fd9" style={styles.x6Menus_morea31b726a_icon_trazado263} preserveAspectRatio="none" viewBox="0 0 99.984619140625 49.9921875" fill="rgba(188, 224, 253, 1)"><SvgPath d="M 0 49.99234771728516 L 49.99234008789063 0 L 99.98468017578125 49.99234771728516 L 0 49.99234771728516 Z"  /></Svg>
            </View>
            <Text data-layer="55c5b761-7729-4110-a5a0-47cd36805e8a" style={styles.x6Menus_morea31b726a_more}>MORE</Text>
        </View>
    </ScrollView>
    );
  }
}

X6Menus.propTypes = {

}

X6Menus.defaultProps = {

}


const styles = StyleSheet.create({
  "x6Menus": {
    "opacity": 1,
    "position": "relative",
    "backgroundColor": "rgba(255, 255, 255, 1)",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 375,
    "height": 667,
    "left": 0,
    "top": 0
  },
  "x6Menus_topsaebbfea4": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "transparent",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 375,
    "height": 166,
    "left": 0,
    "top": 0
  },
  "x6Menus_topsaebbfea4_rectangulo1007": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "rgba(255, 255, 255, 1)",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "borderTopLeftRadius": 0,
    "borderTopRightRadius": 0,
    "borderBottomLeftRadius": 0,
    "borderBottomRightRadius": 0,
    "width": 375,
    "height": 166,
    "left": 0,
    "top": 0
  },
  "x6Menus_topsaebbfea4_icon0f69b727": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "transparent",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 80,
    "height": 80,
    "left": 42,
    "top": 43
  },
  "x6Menus_topsaebbfea4_icon0f69b727_elipse243": {
    "opacity": 1,
    "position": "absolute",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 80,
    "height": 80,
    "left": 0,
    "top": 0
  },
  "x6Menus_topsaebbfea4_icon0f69b727_trazado234": {
    "opacity": 1,
    "position": "absolute",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 40,
    "height": 68.3,
    "left": 40,
    "top": 0
  },
  "x6Menus_topsaebbfea4_tops": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "rgba(255, 255, 255, 0)",
    "color": "rgba(38, 153, 251, 1)",
    "fontSize": 20,
    "fontWeight": "700",
    "fontStyle": "normal",
    /*"fontFamily": "Arial",*/
    "textAlign": "left",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 54,
    "height": 22,
    "left": 162,
    "top": 73
  },
  "x6Menus_bottomsa866578a": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "transparent",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 375,
    "height": 167,
    "left": 0,
    "top": 166
  },
  "x6Menus_bottomsa866578a_rectangulo1008": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "rgba(241, 249, 255, 1)",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "borderTopLeftRadius": 0,
    "borderTopRightRadius": 0,
    "borderBottomLeftRadius": 0,
    "borderBottomRightRadius": 0,
    "width": 375,
    "height": 167,
    "left": 0,
    "top": 0
  },
  "x6Menus_bottomsa866578a_icon8382e0b8": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "transparent",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 92.35,
    "height": 80,
    "left": 36,
    "top": 44
  },
  "x6Menus_bottomsa866578a_icon8382e0b8_trazado235": {
    "opacity": 1,
    "position": "absolute",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 92.35,
    "height": 80,
    "left": 0,
    "top": 0
  },
  "x6Menus_bottomsa866578a_icon8382e0b8_trazado236": {
    "opacity": 1,
    "position": "absolute",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 92.35,
    "height": 80,
    "left": 0,
    "top": 0
  },
  "x6Menus_bottomsa866578a_bottoms": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "rgba(255, 255, 255, 0)",
    "color": "rgba(38, 153, 251, 1)",
    "fontSize": 20,
    "fontWeight": "700",
    "fontStyle": "normal",
    /*"fontFamily": "Arial",*/
    "textAlign": "left",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 100,
    "height": 22,
    "left": 162,
    "top": 73
  },
  "x6Menus_accessories88b62596": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "transparent",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 375,
    "height": 167,
    "left": 0,
    "top": 333
  },
  "x6Menus_accessories88b62596_rectangulo1009": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "rgba(188, 224, 253, 1)",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "borderTopLeftRadius": 0,
    "borderTopRightRadius": 0,
    "borderBottomLeftRadius": 0,
    "borderBottomRightRadius": 0,
    "width": 375,
    "height": 167,
    "left": 0,
    "top": 0
  },
  "x6Menus_accessories88b62596_icon051b936a": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "transparent",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 80,
    "height": 80,
    "left": 42,
    "top": 44
  },
  "x6Menus_accessories88b62596_icon051b936a_rectangulo874": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "rgba(38, 153, 251, 1)",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "borderTopLeftRadius": 0,
    "borderTopRightRadius": 0,
    "borderBottomLeftRadius": 0,
    "borderBottomRightRadius": 0,
    "width": 40,
    "height": 40,
    "left": 0,
    "top": 0
  },
  "x6Menus_accessories88b62596_icon051b936a_rectangulo875": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "rgba(127, 196, 253, 1)",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "borderTopLeftRadius": 0,
    "borderTopRightRadius": 0,
    "borderBottomLeftRadius": 0,
    "borderBottomRightRadius": 0,
    "width": 40,
    "height": 40,
    "left": 40,
    "top": 0
  },
  "x6Menus_accessories88b62596_icon051b936a_rectangulo876": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "rgba(127, 196, 253, 1)",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "borderTopLeftRadius": 0,
    "borderTopRightRadius": 0,
    "borderBottomLeftRadius": 0,
    "borderBottomRightRadius": 0,
    "width": 40,
    "height": 40,
    "left": 0,
    "top": 40
  },
  "x6Menus_accessories88b62596_icon051b936a_rectangulo877": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "rgba(38, 153, 251, 1)",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "borderTopLeftRadius": 0,
    "borderTopRightRadius": 0,
    "borderBottomLeftRadius": 0,
    "borderBottomRightRadius": 0,
    "width": 40,
    "height": 40,
    "left": 40,
    "top": 40
  },
  "x6Menus_accessories88b62596_accessories": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "rgba(255, 255, 255, 0)",
    "color": "rgba(38, 153, 251, 1)",
    "fontSize": 20,
    "fontWeight": "700",
    "fontStyle": "normal",
    /*"fontFamily": "Arial",*/
    "textAlign": "left",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 146,
    "height": 22,
    "left": 162,
    "top": 73
  },
  "x6Menus_morea31b726a": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "transparent",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 375,
    "height": 167,
    "left": 0,
    "top": 500
  },
  "x6Menus_morea31b726a_rectangulo1010": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "rgba(127, 196, 253, 1)",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "borderTopLeftRadius": 0,
    "borderTopRightRadius": 0,
    "borderBottomLeftRadius": 0,
    "borderBottomRightRadius": 0,
    "width": 375,
    "height": 167,
    "left": 0,
    "top": 0
  },
  "x6Menus_morea31b726a_icon": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "transparent",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 100,
    "height": 100.03,
    "left": 32,
    "top": 33
  },
  "x6Menus_morea31b726a_icon_rectangulo1011": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "rgba(38, 153, 251, 1)",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "borderTopLeftRadius": 0,
    "borderTopRightRadius": 0,
    "borderBottomLeftRadius": 0,
    "borderBottomRightRadius": 0,
    "width": 70.71,
    "height": 70.71,
    "left": 14.64,
    "top": 14.68
  },
  "x6Menus_morea31b726a_icon_trazado263": {
    "opacity": 1,
    "position": "absolute",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 99.98,
    "height": 49.99,
    "left": 0.01,
    "top": 0
  },
  "x6Menus_morea31b726a_more": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "rgba(255, 255, 255, 0)",
    "color": "rgba(38, 153, 251, 1)",
    "fontSize": 20,
    "fontWeight": "700",
    "fontStyle": "normal",
    /*"fontFamily": "Arial",*/
    "textAlign": "left",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 60,
    "height": 22,
    "left": 162,
    "top": 73
  }
});