import React, {Component} from 'react';
import PropTypes from "prop-types";
import {StyleSheet, Text, View, TextInput, FlatList, Picker, ScrollView, TouchableHighlight} from 'react-native';
import {Image as ReactImage} from 'react-native';
import Svg, {Defs, Pattern} from 'react-native-svg';
import {Path as SvgPath} from 'react-native-svg';
import {Text as SvgText} from 'react-native-svg';
import {Image as SvgImage} from 'react-native-svg';

export default class X4SignUp extends Component {

  constructor(props) {
      super(props);
      this.state = {
          
      };
  }


  handlePress(target, owner) {
    if (this.props.onPress) {
        let name;
        let id;
        let index = -1;
        if (target.search("::") > -1) {
            const varCount = target.split("::").length;
            if (varCount === 2) {
                name = target.split("::")[0];
                id = target.split("::")[1];
            } else if (varCount === 3) {
                name = target.split("::")[0];
                index = parseInt(target.split("::")[1]);
                id = target.split("::")[2];
            }
        } else {
            name = target;
        }
        this.props.onPress({ type: 'button', name: name, index: index, id: id, owner: owner });
    }
  }

  handleChangeTextinput(name, value) {
      let id;
      let index = -1;
      if (name.search('::') > -1) {
          const varCount = name.split("::").length;
          if (varCount === 2) {
              name = name.split("::")[0];
              id = name.split("::")[1];
          } else if (varCount === 3) {
              name = name.split("::")[0];
              index = name.split("::")[1];
              id = name.split("::")[2];
          }
      } else {
          name = name;
      }
      let state = this.state;
      state[name.split('::').join('')] = value;
      this.setState(state, () => {
          if (this.props.onChange) {
              this.props.onChange({ type: 'textinput', name: name, value: value, index: index, id: id });
          }
      });
  }

  render() {
    
    return (
    <ScrollView data-layer="f9172e44-4807-4018-aeff-27e49c46f64f" style={styles.x4SignUp}>
        <ScrollView data-layer="b36ec954-8d2c-4038-a623-e0b88cd92674" style={styles.x4SignUp_input372452ac}>
            <View data-layer="9a00b89b-4c97-488c-a681-acee63af5584" style={styles.x4SignUp_input372452ac_input35f7d002}>
                <View data-layer="0064da7e-e659-4056-b51a-332ec20e007f" style={styles.x4SignUp_input372452ac_input35f7d002_rectangulo122f20f8cef}></View>
                <Text data-layer="c41def8d-38f3-4c12-a035-a217d05ffd33" style={styles.x4SignUp_input372452ac_input35f7d002_johnDoe}>John Doe</Text>
                <Text data-layer="59f036db-7c1e-4a7e-b449-98d34fb4b052" style={styles.x4SignUp_input372452ac_input35f7d002_fullName}>Full Name</Text>
            </View>
            <View data-layer="0a498010-9840-4749-8d13-f73cbc0ee462" style={styles.x4SignUp_input372452ac_input996974b7}>
                <View data-layer="09ee2995-9598-4cf5-971a-d4a2653db5f1" style={styles.x4SignUp_input372452ac_input996974b7_rectangulo122f7be08b4}></View>
                <Text data-layer="e197b4e6-f3d1-4e36-ab5f-fae861aa0d66" style={styles.x4SignUp_input372452ac_input996974b7_johndoe}>johndoe@mail.com</Text>
                <Text data-layer="a2c55375-a79d-40a6-a186-dd08044ad6af" style={styles.x4SignUp_input372452ac_input996974b7_email}>Email</Text>
            </View>
            <View data-layer="69bc363b-2022-42d3-a649-7ae2b78f0385" style={styles.x4SignUp_input372452ac_inputac79e4c9}>
                <View data-layer="f56beea2-4930-4116-9baf-54bf4d89ec46" style={styles.x4SignUp_input372452ac_inputac79e4c9_rectangulo12299576c1e}></View>
                <Text data-layer="e1e7bb7e-8178-44b9-b4f2-d7403c74b06c" style={styles.x4SignUp_input372452ac_inputac79e4c9_x26b18191}>●●●●●●●</Text>
                <Text data-layer="5e9d2162-1c1e-4467-a2f3-6bb0f692dd40" style={styles.x4SignUp_input372452ac_inputac79e4c9_password}>Password</Text>
            </View>
            <View data-layer="1ba0be16-1dfd-425a-8bb1-a874d7af0eb2" style={styles.x4SignUp_input372452ac_input}>
                <View data-layer="92f58fcf-38b9-4a24-948c-d6fa6499f098" style={styles.x4SignUp_input372452ac_input_rectangulo122}></View>
                <Text data-layer="3ca29dee-7bf6-43f9-87c7-25c950421080" style={styles.x4SignUp_input372452ac_input_x3ceecbcb}>●●●●●●●</Text>
                <Text data-layer="e1cd030a-5b1c-4693-bf37-9a207be77333" style={styles.x4SignUp_input372452ac_input_confirmPassword}>Confirm Password</Text>
            </View>
        </ScrollView>
        <View data-layer="e437cfd0-195c-4121-b243-10ed5ac59116" style={styles.x4SignUp_next}>
            <Svg data-layer="228da68a-3d89-4a72-90ba-a3725544ab3c" style={styles.x4SignUp_next_trazado655} preserveAspectRatio="none" viewBox="0 0 327 48" fill="rgba(112, 152, 228, 1)"><SvgPath d="M 4 0 L 323 0 C 325.2091369628906 0 327 1.790860891342163 327 4 L 327 44 C 327 46.20914077758789 325.2091369628906 48 323 48 L 4 48 C 1.790860891342163 48 0 46.20914077758789 0 44 L 0 4 C 0 1.790860891342163 1.790860891342163 0 4 0 Z"  /></Svg>
            <View data-layer="92866cf8-a296-4d89-900d-8d57fbc9c327" style={styles.x4SignUp_next_symbol181}>
                <Svg data-layer="9dce7615-eb34-4f1c-8205-66fd4d830e96" style={styles.x4SignUp_next_symbol181_trazado104b859085} preserveAspectRatio="none" viewBox="0 0 16 16" fill="rgba(255, 255, 255, 1)"><SvgPath d="M 8 0 L 6.545454978942871 1.454545497894287 L 12.05194854736328 6.961039066314697 L 0 6.961039066314697 L 0 9.038961410522461 L 12.05194854736328 9.038961410522461 L 6.545454978942871 14.54545497894287 L 8 16 L 16 8 L 8 0 Z"  /></Svg>
            </View>
        </View>
        <View data-layer="c371fa68-a20a-4402-adbc-935c20cf867f" style={styles.x4SignUp_backwardArrow}>
            <Svg data-layer="44b267a5-6091-40bd-90f4-7489044edfb8" style={styles.x4SignUp_backwardArrow_trazado10} preserveAspectRatio="none" viewBox="0 0 16 16" fill="rgba(38, 153, 251, 1)"><SvgPath d="M 8 0 L 6.545454978942871 1.454545497894287 L 12.05194854736328 6.961039066314697 L 0 6.961039066314697 L 0 9.038961410522461 L 12.05194854736328 9.038961410522461 L 6.545454978942871 14.54545497894287 L 8 16 L 16 8 L 8 0 Z"  /></Svg>
        </View>
        <ReactImage data-layer="b464048d-4e16-4c8d-a802-5543269bc3c0" source={require('./assets/mind.png')} style={styles.x4SignUp_mind} />
    </ScrollView>
    );
  }
}

X4SignUp.propTypes = {

}

X4SignUp.defaultProps = {

}


const styles = StyleSheet.create({
  "x4SignUp": {
    "opacity": 1,
    "position": "relative",
    "backgroundColor": "rgba(241, 249, 255, 1)",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 375,
    "height": 812,
    "left": 0,
    "top": 0
  },
  "x4SignUp_input372452ac": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "transparent",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 327,
    "height": 316,
    "left": 24,
    "top": 133
  },
  "x4SignUp_input372452ac_input35f7d002": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "transparent",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 327,
    "height": 65,
    "left": 0,
    "top": 2
  },
  "x4SignUp_input372452ac_input35f7d002_rectangulo122f20f8cef": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "rgba(255, 255, 255, 1)",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "borderTopWidth": 1,
    "borderTopColor": "rgba(188, 224, 253, 1)",
    "borderRightWidth": 1,
    "borderRightColor": "rgba(188, 224, 253, 1)",
    "borderBottomWidth": 1,
    "borderBottomColor": "rgba(188, 224, 253, 1)",
    "borderLeftWidth": 1,
    "borderLeftColor": "rgba(188, 224, 253, 1)",
    "borderTopLeftRadius": 0,
    "borderTopRightRadius": 0,
    "borderBottomLeftRadius": 0,
    "borderBottomRightRadius": 0,
    "width": 327,
    "height": 48,
    "left": 0,
    "top": 17
  },
  "x4SignUp_input372452ac_input35f7d002_johnDoe": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "rgba(255, 255, 255, 0)",
    "color": "rgba(38, 153, 251, 1)",
    "fontSize": 14,
    "fontWeight": "400",
    "fontStyle": "normal",
    /*"fontFamily": "Arial",*/
    "textAlign": "left",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 60,
    "height": 16,
    "left": 20,
    "top": 33
  },
  "x4SignUp_input372452ac_input35f7d002_fullName": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "rgba(255, 255, 255, 0)",
    "color": "rgba(38, 153, 251, 1)",
    "fontSize": 10,
    "fontWeight": "400",
    "fontStyle": "normal",
    /*"fontFamily": "Arial",*/
    "textAlign": "left",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 46,
    "height": 11,
    "left": 0,
    "top": 0
  },
  "x4SignUp_input372452ac_input996974b7": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "transparent",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 327,
    "height": 65,
    "left": 0,
    "top": 85
  },
  "x4SignUp_input372452ac_input996974b7_rectangulo122f7be08b4": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "rgba(255, 255, 255, 1)",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "borderTopWidth": 1,
    "borderTopColor": "rgba(188, 224, 253, 1)",
    "borderRightWidth": 1,
    "borderRightColor": "rgba(188, 224, 253, 1)",
    "borderBottomWidth": 1,
    "borderBottomColor": "rgba(188, 224, 253, 1)",
    "borderLeftWidth": 1,
    "borderLeftColor": "rgba(188, 224, 253, 1)",
    "borderTopLeftRadius": 0,
    "borderTopRightRadius": 0,
    "borderBottomLeftRadius": 0,
    "borderBottomRightRadius": 0,
    "width": 327,
    "height": 48,
    "left": 0,
    "top": 17
  },
  "x4SignUp_input372452ac_input996974b7_johndoe": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "rgba(255, 255, 255, 0)",
    "color": "rgba(38, 153, 251, 1)",
    "fontSize": 14,
    "fontWeight": "400",
    "fontStyle": "normal",
    /*"fontFamily": "Arial",*/
    "textAlign": "left",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 121,
    "height": 16,
    "left": 19,
    "top": 33
  },
  "x4SignUp_input372452ac_input996974b7_email": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "rgba(255, 255, 255, 0)",
    "color": "rgba(38, 153, 251, 1)",
    "fontSize": 10,
    "fontWeight": "400",
    "fontStyle": "normal",
    /*"fontFamily": "Arial",*/
    "textAlign": "left",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 25,
    "height": 11,
    "left": 0,
    "top": 0
  },
  "x4SignUp_input372452ac_inputac79e4c9": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "transparent",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 327,
    "height": 65,
    "left": 0,
    "top": 168
  },
  "x4SignUp_input372452ac_inputac79e4c9_rectangulo12299576c1e": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "rgba(255, 255, 255, 1)",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "borderTopWidth": 1,
    "borderTopColor": "rgba(188, 224, 253, 1)",
    "borderRightWidth": 1,
    "borderRightColor": "rgba(188, 224, 253, 1)",
    "borderBottomWidth": 1,
    "borderBottomColor": "rgba(188, 224, 253, 1)",
    "borderLeftWidth": 1,
    "borderLeftColor": "rgba(188, 224, 253, 1)",
    "borderTopLeftRadius": 0,
    "borderTopRightRadius": 0,
    "borderBottomLeftRadius": 0,
    "borderBottomRightRadius": 0,
    "width": 327,
    "height": 48,
    "left": 0,
    "top": 17
  },
  "x4SignUp_input372452ac_inputac79e4c9_x26b18191": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "rgba(255, 255, 255, 0)",
    "color": "rgba(38, 153, 251, 1)",
    "fontSize": 14,
    "fontWeight": "400",
    "fontStyle": "normal",
    /*"fontFamily": "Arial",*/
    "textAlign": "left",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 59,
    "height": 16,
    "left": 20,
    "top": 33
  },
  "x4SignUp_input372452ac_inputac79e4c9_password": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "rgba(255, 255, 255, 0)",
    "color": "rgba(38, 153, 251, 1)",
    "fontSize": 10,
    "fontWeight": "400",
    "fontStyle": "normal",
    /*"fontFamily": "Arial",*/
    "textAlign": "left",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 44,
    "height": 11,
    "left": 0,
    "top": 0
  },
  "x4SignUp_input372452ac_input": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "transparent",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 327,
    "height": 65,
    "left": 0,
    "top": 251
  },
  "x4SignUp_input372452ac_input_rectangulo122": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "rgba(255, 255, 255, 1)",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "borderTopWidth": 1,
    "borderTopColor": "rgba(188, 224, 253, 1)",
    "borderRightWidth": 1,
    "borderRightColor": "rgba(188, 224, 253, 1)",
    "borderBottomWidth": 1,
    "borderBottomColor": "rgba(188, 224, 253, 1)",
    "borderLeftWidth": 1,
    "borderLeftColor": "rgba(188, 224, 253, 1)",
    "borderTopLeftRadius": 0,
    "borderTopRightRadius": 0,
    "borderBottomLeftRadius": 0,
    "borderBottomRightRadius": 0,
    "width": 327,
    "height": 48,
    "left": 0,
    "top": 17
  },
  "x4SignUp_input372452ac_input_x3ceecbcb": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "rgba(255, 255, 255, 0)",
    "color": "rgba(38, 153, 251, 1)",
    "fontSize": 14,
    "fontWeight": "400",
    "fontStyle": "normal",
    /*"fontFamily": "Arial",*/
    "textAlign": "left",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 59,
    "height": 16,
    "left": 20,
    "top": 33
  },
  "x4SignUp_input372452ac_input_confirmPassword": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "rgba(255, 255, 255, 0)",
    "color": "rgba(38, 153, 251, 1)",
    "fontSize": 10,
    "fontWeight": "400",
    "fontStyle": "normal",
    /*"fontFamily": "Arial",*/
    "textAlign": "left",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 82,
    "height": 11,
    "left": 0,
    "top": 0
  },
  "x4SignUp_next": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "transparent",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 327,
    "height": 48,
    "left": 24,
    "top": 738
  },
  "x4SignUp_next_trazado655": {
    "opacity": 1,
    "position": "absolute",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 327,
    "height": 48,
    "left": 0,
    "top": 0
  },
  "x4SignUp_next_symbol181": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "transparent",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 16,
    "height": 16,
    "left": 156,
    "top": 16
  },
  "x4SignUp_next_symbol181_trazado104b859085": {
    "opacity": 1,
    "position": "absolute",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 16,
    "height": 16,
    "left": 0,
    "top": 0
  },
  "x4SignUp_backwardArrow": {
    "opacity": 1,
    "position": "absolute",
    "backgroundColor": "transparent",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 16,
    "height": 16,
    "left": 18,
    "top": 29
  },
  "x4SignUp_backwardArrow_trazado10": {
    "opacity": 1,
    "position": "absolute",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 16,
    "height": 16,
    "left": 0,
    "top": 0
  },
  "x4SignUp_mind": {
    "opacity": 1,
    "position": "absolute",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "width": 198.29,
    "height": 209.35,
    "left": 82.84,
    "top": 471.65
  },
  "x4SignUp_mind_trazado643": {
    "opacity": 1,
    "position": "absolute",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 198.29,
    "height": 209.35,
    "left": 0,
    "top": 0
  },
  "x4SignUp_mind_trazado644": {
    "opacity": 1,
    "position": "absolute",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 106.63,
    "height": 209.35,
    "left": 91.66,
    "top": 0
  },
  "x4SignUp_mind_trazado645": {
    "opacity": 1,
    "position": "absolute",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 58.95,
    "height": 68.65,
    "left": 91.66,
    "top": 42.93
  },
  "x4SignUp_mind_trazado646": {
    "opacity": 1,
    "position": "absolute",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 58.95,
    "height": 68.65,
    "left": 32.72,
    "top": 42.93
  },
  "x4SignUp_mind_trazado647": {
    "opacity": 1,
    "position": "absolute",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 39.26,
    "height": 30.72,
    "left": 72.03,
    "top": 47.84
  },
  "x4SignUp_mind_trazado648": {
    "opacity": 1,
    "position": "absolute",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 78.55,
    "height": 35.62,
    "left": 52.39,
    "top": 111.58
  },
  "x4SignUp_mind_trazado649": {
    "opacity": 1,
    "position": "absolute",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 39.28,
    "height": 35.62,
    "left": 91.66,
    "top": 111.58
  },
  "x4SignUp_mind_trazado650": {
    "opacity": 1,
    "position": "absolute",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 39.3,
    "height": 38.03,
    "left": 72.01,
    "top": 73.6
  },
  "x4SignUp_mind_trazado651": {
    "opacity": 1,
    "position": "absolute",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 19.65,
    "height": 38.03,
    "left": 91.66,
    "top": 73.6
  },
  "x4SignUp_mind_trazado652": {
    "opacity": 1,
    "position": "absolute",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 19.63,
    "height": 30.72,
    "left": 91.66,
    "top": 47.84
  },
  "x4SignUp_mind_trazado653": {
    "opacity": 1,
    "position": "absolute",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 39.3,
    "height": 37.98,
    "left": 91.66,
    "top": 78.56
  },
  "x4SignUp_mind_trazado654": {
    "opacity": 1,
    "position": "absolute",
    "marginTop": 0,
    "marginRight": 0,
    "marginBottom": 0,
    "marginLeft": 0,
    "paddingTop": 0,
    "paddingRight": 0,
    "paddingBottom": 0,
    "paddingLeft": 0,
    "width": 39.3,
    "height": 37.98,
    "left": 52.37,
    "top": 78.56
  }
});